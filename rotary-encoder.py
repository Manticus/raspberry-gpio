#!/usr/bin/python

import RPi.GPIO as GPIO
import time

print "starting..."

pin_id = 23
pin2_id = 24

wheel_diameter = 5 #cm
nb_holes_in_wheel = 4

nb_states_by_rotation = float(nb_holes_in_wheel * 2)
pi = 3.14
wheel_perimeter = pi * wheel_diameter

GPIO.setmode(GPIO.BCM)
GPIO.setup(pin_id, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(pin2_id, GPIO.IN, pull_up_down=GPIO.PUD_UP)

last_state = 0
nb_states = 0

previous_second = 0
previous_nb_states = 0
is_direction_A = True

try:
  while True:
    input_state = GPIO.input(pin_id)
    input2_state = GPIO.input(pin2_id)

    if input_state != last_state:
      if input_state == input2_state:
        nb_states -= 1
        if is_direction_A:
          is_direction_A = False
          print('direction changed')
      else:
        nb_states += 1
        if not is_direction_A:
          is_direction_A = True
          print('direction changed')
      last_state = input_state

    current_second = time.strftime('%S')
    if current_second != previous_second:
      if nb_states != previous_nb_states:
        previous_nb_states = nb_states
        nb_rotation = 0
        if previous_nb_states != 0:
          nb_rotation = previous_nb_states / nb_states_by_rotation
        speed_second = nb_rotation * wheel_perimeter
        print ('nb rotation: ' + str(nb_rotation) + '; speed: ' + str(speed_second) + " cm/s")
      previous_second = current_second
      nb_states = 0

    time.sleep(0.01)

except:
  GPIO.cleanup()