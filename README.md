# LED Control
[led-control.py](./led-control.py)

Demo on YouTube:

[![Demo led control.py](https://i.ytimg.com/vi/XB3Bc4hOLQw/hqdefault.jpg?sqp=-oaymwEZCPYBEIoBSFXyq4qpAwsIARUAAIhCGAFwAQ==&rs=AOn4CLDUcoHMCeeQI5VLULOzOB5LBaCXqA)](https://www.youtube.com/watch?v=XB3Bc4hOLQw)

___
# Rotary encoder
[rotary-encoder.py](./rotary-encoder.py)

![Print part](https://cdn.thingiverse.com/renders/4a/fc/eb/6c/6c/d2b5ca33bd970f64a6301fa75ae2eb22_display_large.jpg "Rotary encoder")

[Print part in Thingiverse](https://www.thingiverse.com/thing:2846521)

___
# Relay control with jeedom
[gpio-jeedom-out.py](./gpio-jeedom-out.py)

Create a HTTP interface to turn on/off relay with a Jeedom interface

How to use:

1. Edit pin ids in python file

2. With plugin "Script" create commands (2 actions and 1 info)

```script
Name: On 1
Type script: HTTP
Type: Action / Defaut
Request: http://<raspberry ip>/<pin id>/on

Name: Off 1
Type script: HTTP
Type: Action / Defaut
Request: http://<raspberry ip>/<pin id>/off

Name: State 1
Type script: JSON
Type: Info / Numeric
Request: <pin id> > state
URL: http://<raspberry ip>
```
