#!/usr/bin/python

import spidev
import requests
import time
import json
import os
import RPi.GPIO as GPIO

jeedom = "192.168.0.10" # jeedom url
jeedom_api_key = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx" # api key for jeedom
tolerency = 10 # if potentiometers are not really stable, 10 is a good value to limit requests (0 = extra sensibility)
reset_pin = 18 # the push button ton reset min/max value (if the value is > 300: reset the max value, else reset min value)

max_r = max_g = max_b = 1000
min_r = min_g = min_b = 0

file_config = 'led-control.conf' # filename to import/export config for min/max values
if os.path.isfile(file_config):
    json_data = open(file_config).read()
    if len(json_data) > 0:
        conf = json.loads(json_data)
        max_r = conf['max_r']
        max_g = conf['max_g']
        max_b = conf['max_b']
        min_r = conf['min_r']
        min_g = conf['min_g']
        min_b = conf['min_b']

spi = spidev.SpiDev()
spi.open(0,0)  # pin CE0, spi.open(0,1) for pin CE1
spi.max_speed_hz = 1350000

# read value from MCP3008
def ReadChannel(adcnum):
    if((adcnum > 7) or (adcnum < 0)):
        return -1
    r = spi.xfer2([1,(8+adcnum)<<4,0])
    adcout = (r[1]*256) + r[2]
    return adcout

# get value between 0 and 255
def get_256_value(value, value_min, value_max):
    if value > value_max:
        value = value_max
    if value < value_min:
        value = value_min
    limit = value_max - value_min
    current_value = value - value_min
    return int(float(current_value) / float(limit) * 255.0)
 
# call api jeedom to change color
def send(hexa):
    print hexa
    url = "http://" + jeedom + "/core/api/jeeApi.php?apikey=" + jeedom_api_key + "&type=cmd&id=1&color=%23" + hexa
    requests.get(url)

GPIO.setmode(GPIO.BCM)
# reset button
GPIO.setup(reset_pin, GPIO.IN, pull_up_down=GPIO.PUD_UP)

previous_value1_r = previous_value2_g = previous_value3_b = 0
value_w = 0
previous_reset_button = 1
previous_hexa = '00000000'
has_changed = False

while True:
    value1_r = ReadChannel(0)
    value2_g = ReadChannel(1)
    value3_b = ReadChannel(2)

    if GPIO.input(reset_pin) == 0:
        if previous_reset_button == 1:
            previous_reset_button = 0
            if value1_r > 300:
                max_r = value1_r
            else:
                min_r = value1_r
            if value2_g > 300:
                max_g = value2_g
            else:
                min_g = value2_g
            if value3_b > 300:
                max_b = value3_b
            else:
                min_b = value3_b
            config = {
                'max_r': max_r, 'max_g': max_g, 'max_b': max_b,
                'min_r': min_r, 'min_g': min_g, 'min_b': min_b
            }
            print "Save new config: ", config
            with open(file_config, 'w') as outfile:
                json.dump(config, outfile)
    elif previous_reset_button == 0:
            previous_reset_button = 1

    if previous_value1_r - tolerency > value1_r  or previous_value1_r + tolerency < value1_r:
        previous_value1_r = value1_r
        has_changed = True

    if previous_value2_g - tolerency > value2_g or previous_value2_g + tolerency < value2_g:
        previous_value2_g = value2_g
        has_changed = True

    if previous_value3_b - tolerency > value3_b or previous_value3_b + tolerency < value3_b:
        previous_value3_b = value3_b
        has_changed = True

    if has_changed:
        print "real : r: ", value1_r, "g:", value2_g, "b:", value3_b
        
        value1_r_256 = get_256_value(value1_r, min_r, max_r)
        value2_g_256 = get_256_value(value2_g, min_g, max_g)
        value3_b_256 = get_256_value(value3_b, min_b, max_b)
        
        hexa = "%02x%02x%02x%02x" % (value1_r_256, value2_g_256, value3_b_256, value_w)
        if previous_hexa != hexa:
            send(hexa)
            previous_hexa = hexa
        has_changed = False

    
    time.sleep(1)
