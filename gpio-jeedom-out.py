#!/usr/bin/env python

# requirement:
# sudo pip install flask

import RPi.GPIO as GPIO
import json
from flask import Flask
app = Flask(__name__)

GPIO.setmode(GPIO.BCM)

pins = {
  24 : {'name' : 'Relai 1'},
  18 : {'name' : 'Relai 2'},
  23 : {'name' : 'Relai 3'},
  25 : {'name' : 'Relai 4'},
   4 : {'name' : 'Relai 5'},
  17 : {'name' : 'Relai 6'},
  26 : {'name' : 'Relai 7'},
  16 : {'name' : 'Relai 8'},
  21 : {'name' : 'Relai 9'}
}

for pin in pins:
  GPIO.setup(pin, GPIO.OUT)

@app.route("/")
def main():
  for pin in pins:
    pins[pin]['state'] = GPIO.input(pin)
  return json.dumps(pins, ensure_ascii=False)

@app.route("/<changePin>/<action>")
def action(changePin, action):
  changePin = int(changePin)

  if action == "on":
    GPIO.output(changePin, GPIO.HIGH)
  if action == "off":
    GPIO.output(changePin, GPIO.LOW)

  pins[pin]['state'] = GPIO.input(pin)
  return json.dumps(pins, ensure_ascii=False)

if __name__ == "__main__":
   app.run(host='0.0.0.0', port=80, debug=True)
